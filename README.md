# Quick start guide for Gameboy development

This guide is there to remind me how to setup an environement.

- GBDK
- Msys2 for `make`
- Visual Studio Code (optional)

The provided template is based on the minimal template of GBDK.

## Steps

- Download the last version of **GBDK** https://github.com/gbdk-2020/gbdk-2020
    - Decompress the archive and put the gbdk folder somewhere (root of C: for example)
- Get the `make` command on windows throught **Msys2** :
    - Download and install **Msys2**
    - in Msys2 : `pacman -S make` to install make
- Edit environement variables :
    - Add a **'gbdk'** system varialble with the path to the gbdk root folder
    - Add the path to the `make` executable folder in the system path variable (should be `msys64\usr\bin`) 
    - From now on you should be able to run `make` in VsCode terminal
- _Optionnal :_ VsCode
    - Install the C/C++ extention
    - In the config file, add "${gbdk}/**" in the includePath
        - This helps VsCode locate your gbdk folder for ✨**intellisense**✨

- Download the template GB project found in this repo
- Try to run `make` on the project, this should generate a working blank .gb file
